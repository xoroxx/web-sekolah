-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 14, 2018 at 03:27 AM
-- Server version: 10.1.29-MariaDB-6
-- PHP Version: 7.2.2-1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `al-munir`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `admin_name` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `username` varchar(35) COLLATE latin1_general_ci NOT NULL,
  `password` varchar(35) COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_name`, `username`, `password`) VALUES
(1, 'Administrator', 'admin', '21232f297a57a5a743894a0e4a801fc3'),
(3, 'user', 'user', 'ee11cbb19052e40b07aac0ca060c23ee');

-- --------------------------------------------------------

--
-- Table structure for table `info`
--

CREATE TABLE `info` (
  `info_id` int(11) NOT NULL,
  `info_title` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `info_content` text COLLATE latin1_general_ci NOT NULL,
  `info_tanggal` date NOT NULL,
  `info_image` text COLLATE latin1_general_ci NOT NULL,
  `admin_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `info`
--

INSERT INTO `info` (`info_id`, `info_title`, `info_content`, `info_tanggal`, `info_image`, `admin_id`) VALUES
(2, 'Pemenang Lomba Sekolah Sehat (LSS) Tingkat TK/RA Tahun 2015', '<div align=\"justify\">Pemenang Lomba Sekolah Sehat (LSS) Tingkat TK/RA Tahun 2015 di raih oleh TK. Telkom Padang Kota Padang yang mana hadiah berupa uang pembinaan dan tropy langsung diserahkan oleh Bapak Reydonnyzar Moenek Pj. Gubernur Sumatera Barat ke Pada Kepala TK Telkom Padang Ibu Irma Candra Dewi, SPd tanggal 30 November 2015 dihalaman Kantor Gubernur Sumatera Barat Padang.</div>', '2017-12-28', '6', 1),
(3, 'sasasa', 'sasasa<br>', '2018-01-17', '164792.', 1);

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `page_id` int(11) NOT NULL,
  `page_title` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `page_content` text COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`page_id`, `page_title`, `page_content`) VALUES
(2, 'Profil', '<div align=\"justify\"><b>Sejarah :</b></div><div align=\"justify\">jsdksdsdjksdks</div><div align=\"justify\"><br></div><div align=\"justify\"><b>Visi dan Misi :</b><br></div><div align=\"justify\">- Visi <br></div><div align=\"justify\">\"Unggul dan IMTAQ dan IPTEK\"</div><div align=\"justify\">- Misi</div><ol><li>Mewujudkan ajaran-ajaran dan nilai ajaran Islam sebagai pandangan Islam dan sikap hidup dalam kehidupan sehari-hari.</li><li>hghghg</li></ol>');

-- --------------------------------------------------------

--
-- Table structure for table `tamu`
--

CREATE TABLE `tamu` (
  `id_tamu` int(11) NOT NULL,
  `nama_tamu` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `email_tamu` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `alamat_tamu` varchar(250) COLLATE latin1_general_ci NOT NULL,
  `judul_pesan` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `pesan` text COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `tamu`
--

INSERT INTO `tamu` (`id_tamu`, `nama_tamu`, `email_tamu`, `alamat_tamu`, `judul_pesan`, `pesan`) VALUES
(1, 'jhkjhhkj', 'ytjjj@gj.bonmnj', 'hjhjh', 'hgjhj', 'hjhjh'),
(2, 'tgrtgr', 'grgrg@fdf.co', 'gf', 'gfg', 'fgfgf');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `info`
--
ALTER TABLE `info`
  ADD PRIMARY KEY (`info_id`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `tamu`
--
ALTER TABLE `tamu`
  ADD PRIMARY KEY (`id_tamu`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `info`
--
ALTER TABLE `info`
  MODIFY `info_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tamu`
--
ALTER TABLE `tamu`
  MODIFY `id_tamu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
