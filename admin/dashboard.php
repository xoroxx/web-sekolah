<?php
    session_start();
	error_reporting(0);
    if (!isset($_SESSION['MODGOD'])){
        header('location: ./');
    } else {
        include("include/mysql.php");
        include("include/seal.php");
		include("include/thumb.php");
		include("include/date.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pl" xml:lang="pl">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Admin Panel</title>
<link rel="shortcut icon" href="img/icon.jpg">
<link rel="stylesheet" type="text/css" href="css/style.css" media="screen" />
<link rel="stylesheet" type="text/css" href="css/navi.css" media="screen" />
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="js/nicEdit.js"></script>
<script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
</script>
</head>
<body>
<div class="wrap">
	<div id="header">
		<div id="top">
			<div class="left">
				<h2 id="admin" >Admin Panel</h2>
			</div>
			<div class="right">
				<p><strong><?php echo $_SESSION['name']; ?></strong> [ <a href="?module=logout">Logout</a> ]</p>
			</div>
		</div>
		<div id="nav">
			<ul>
				<li class="upp"><a href="?module=main">Beranda</a></li>
				<li class="upp"><a href="?module=info">Info</a></li>
				<li class="upp"><a href="?module=page">Page</a></li>
				<li class="upp"><a href="?module=inbox">Buku Tamu</a></li>
				<li class="upp"><a href="?module=admin">Admin</a></li>
			</ul>
		</div>
	</div>
	<div id="content">
		<div id="main">
			<?php
				if (isset($_GET['module'])){
					$module = $_GET['module'];
					if ($module == 'main'){
						include("module/main/main.php");
					} elseif ($module == 'info'){
						include  'module/info/info.php';
					}elseif ($module == 'page'){
						include  'module/page/page.php';
					} elseif ($module == 'inbox'){
						include  'module/inbox/inbox.php';
					} elseif ($module == 'admin'){
						include  'module/admin/admin.php';
					} elseif ($module == 'logout'){
						session_destroy();
						ob_end_clean();
						header("location: ../");
					} else {
						header('location: index.php');
					}
				} else {
					header('location: index.php');
				}
			?>
		</div>
	</div>
	<div id="footer">
		<div class="left">
			<p><strong>Copyright &copy; 2018</strong> - All Rights Reserved</p>
		</div>
		<div class="right">
			<p><strong><a href="?module=main">Admin Panel</a></strong></p>
		</div>
	</div>
</div>
</body>
</html>
<?php
    }
?>